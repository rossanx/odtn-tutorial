# ODTN TUTORIAL - PART 1 - BASICS

- Tested platforms:
  - Archlinux 64 bits - native (2021-05-01, 2018-11-01, 2021-03-01)
  - Archlinux 64 bits - VirtualBox (2021-05-01)
  - Debian 64 bits version 10.9 - VirtualBox


- Install bazel version 3.7.2
- Create git-repos directory
```console
mkdir ~/git-repos
```

Also, it's highly recommended to **prune** (a less drastic alternative would be ```docker rm <CONTAINER_ID>``` and ```docker rmi <IMAGE_ID>```) old docker images and containers related to ODTN-emulator. Usually these images are:

- odtn-emulator_tapi_ols  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp; latest
- odtn-emulator_openconfig_cassini_1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; latest
- odtn-emulator_openconfig_cassini_2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; latest
- ubuntu  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; 18.04


## Download and Compile ONOS

```console
cd ~/git-repos
git clone https://github.com/opennetworkinglab/onos.git
cd onos
git checkout 53733d577128d3fc2978d814afb04a3e692d37c9
export ONOS_APPS=odtn-service,roadm,gui2,optical-rest
bazel build onos
```

## Running ONOS

```console
cd ~/git-repos/onos
export ONOS_APPS=odtn-service,roadm,gui2,optical-rest
bazel run onos-local -- clean
```

## Download and Fix ODTN-emulator Dockerfile

```console
cd ~/git-repos
git clone https://github.com/opennetworkinglab/ODTN-emulator.git
cd ODTN-emulator
git checkout faef96091a822bd9783ace9887f851c850de021b
sed -i 's+git clone git://git.cryptomilk.org+git clone https://git.cryptomilk.org+g' emulator-oc-cassini/Dockerfile
sed -i 's+cd Netopeer2/server \&\& git checkout v0.7-r1+cd Netopeer2 \&\& git checkout v0.7-r1 \&\& cd server+g' emulator-oc-cassini/Dockerfile
```

## Running ODTN-emulator

```console
cd ~/git-repos/ODTN-emulator
docker-compose up -d
```

## Uploading configuration to ONOS

```console
~/git-repos/onos/tools/package/runtime/bin/onos-netcfg localhost ~/git-repos/ODTN-emulator/topo/with-rest-tapi/device.json
~/git-repos/onos/tools/package/runtime/bin/onos-netcfg localhost ~/git-repos/ODTN-emulator/topo/with-rest-tapi/link.json
```

## See results

Open a web browser and type the following URL
```
http://localhost:8181/onos/ui
```

Credentials:
```
USERNAME: onos
PASSWORD: rocks
```

# ODTN TUTORIAL - PART 2 - NETCONF

<!--
In debian:
   sudo apt-get install python3-pip
   pip3 install netconf-console
-->

#### Installation

1. Install netconf-console system-wide (make sure pip3 is installed on your distro)
```
sudo pip3 install netconf-console
```

2. Change ncclient to version 0.6.3 
> Delay this change a little bit: if next step (3) works, just ignore this.
> Tested versions: ncclient==0.6.7, netconf-console==2.3.0

```
sudo pip3 install ncclient=0.6.3
```

3. Exchange hellos to test `netconf-console` installation
```
netconf-console --host localhost --port 11002 -u root -p root --hello
```


#### Inspecting NETCONF message exchange between ONOS and ODTN-emulator

1. Log into ONOS cli
```
ssh -p 8101 onos@localhost
```
> On the firt login you have to answer 'yes' to the question 'The authenticity of host .....'
> After answering 'yes', provide the password 'rocks'.
>
> If you want to save the session to a file for further investigation, type instead
>     `ssh -p 8101 onos@localhost | tee onos.netconf.output`
> 
> After exiting the session, or even from another terminal, you can open file onos.netconf.output and inspect all messages, including log outputs (2.). WARNING: this file can increse in size really fast, so be cautious about disk usage.
>

2. Enable log output from NETCONF
```
onos@root > log:set TRACE org.onosproject.netconf
onos@root > log:set TRACE org.onosproject.drivers.netconf
onos@root > log:tail
```

3. NETCONF hello messages

- Try to find NETCONF hello messages, they are exchanged periodically. The request should look like the following:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<hello xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
  <capabilities>
    <capability>urn:ietf:params:netconf:base:1.0</capability>
    <capability>urn:ietf:params:netconf:base:1.1</capability>
  </capabilities>
</hello>
```

- The response is too big to put it here, so here's an excerpt:
```xml
<hello xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
  <capabilities>
     <capability>urn:ietf:params:netconf:base:1.0</capability>
     <capability>urn:ietf:params:netconf:base:1.1</capability>
     <capability>urn:ietf:params:netconf:capability:writable-running:1.0</capability>
     <capability>urn:ietf:params:netconf:capability:candidate:1.0</capability>
     <capability>urn:ietf:params:netconf:capability:rollback-on-error:1.0</capability>
     <capability>urn:ietf:params:netconf:capability:validate:1.1</capability>
     <capability>urn:ietf:params:netconf:capability:startup:1.0</capability>
     <capability>urn:ietf:params:netconf:capability:url:1.0</capability>
     ...
```

4. ONOS and NETCONF

- Now interact with ONOS and observe ONOS logs. Open ONOS URL:
```
http://localhost:8181/onos/ui/#/roadm-gui/roadm-port-gui?devId=netconf:127.0.0.1:11003
```
- Select cassin1 and click the upper right PORT icon, just like the following image

  ![ONOS Roadm GUI App](/images/roadm-gui.png)

- Observe the ONOS log output from the cli (ssh session) - it scrolls really fast, you would probably need to analyze the output offline (onos.netconf.output). This is how a request for input-power-range should look like:

```xml
#374
<?xml version="1.0" encoding="UTF-8"?>
<rpc xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" message-id="720">
  <get>
    <filter>
      <components xmlns="http://openconfig.net/yang/platform">
        <component>
          <name>xe6/1</name>
          <optical-channel xmlns="http://openconfig.net/yang/terminal-device">
            <state>
              <input-power-range/>
	    </state>
          </optical-channel>
        </component>
      </components>
    </filter>
  </get>
</rpc>
##
```
> Can you guess what that `#374 ` at the beginning and the `##` at the end mean?
> Tip: It's related to framing.

#### Using NETCONF from cli (direct communication with ODTN-emulator)

Make NETCONF RPC calls to ODTN-emulator using XML files

```
cd ~/git-repos/ODTN-emulator
```

```
netconf-console --host=127.0.0.1 --port=11002 -u root -p root --rpc=emulator-test/get-terminal-device.xml
```

```
netconf-console --host=127.0.0.1 --port=11003 -u root -p root --rpc=emulator-test/get-terminal-device.xml
```


Make NETCONF RPC calls to ODTN-emulator: --get

- Return the entire NETCONF tree without filtering
```
netconf-console --host 192.168.15.116 --port 11002 -u root -p root  --get
```


# ODTN TUTORIAL - PART 3 - TAPI

#### Using TAPI from cli (direct communication with ODTN-emulator)

```
curl http://localhost:11000/restconf/data/tapi-common:context
```


# ODTN TUTORIAL - PART 4 - XPath

## No namespaces - offline exploration
1. Obtain a fresh XML NETCONF tree
```
netconf-console --host 192.168.15.116 --port 11002 -u root -p root  --get > odtn-emulator-all.xml
```

2. Remove namespaces from XML file
```
sed 's/xmlns=/ignore=/g' odtn-emulator-all.xml > odtn-emulator-all-NO-NAMESPACE.xml
```
> This is a straightforward method to explore XML with xmllint and namespaces. Another strategy is to use the technique with *[loca-name()=..., which is too verbose and cumbersome.

#### Filter all components
```
xmllint --xpath "//component" odtn-emulator-all-NO-NAMESPACE.xml
```
> It's too big to provide the output here


#### Filter a single component
```
xmllint --xpath "//components/component[name='oe1']" odtn-emulator-all-NO-NAMESPACE.xml
```
Here's the output
```xml
<component>
  <name>oe1</name>
  <config>
    <name>oe1</name>
  </config>
  <state>
    <type xmlns:oc-platform-types="http://openconfig.net/yang/platform-types">oc-platform-types:TRANSCEIVER</type>
    <empty>false</empty>
  </state>
  <port/>
  <transceiver ignore="http://openconfig.net/yang/platform/transceiver">
    <config>
      <enabled>true</enabled>
      <form-factor-preconf xmlns:oc-opt-types="http://openconfig.net/yang/transport-types">oc-opt-types:CFP2_ACO</form-factor-preconf>
    </config>
  </transceiver>
</component>
```

#### Filter a single component configuration value
```
xmllint --xpath "//components/component[name='oe1']/state" odtn-emulator-all-NO-NAMESPACE.xml
```

Here's the output
```xml
<state>
    <type xmlns:oc-platform-types="http://openconfig.net/yang/platform-types">oc-platform-types:TRANSCEIVER</type>
    <empty>false</empty>
</state>
```


# ODTN TUTORIAL - PART X - ADVANCED

## SYSREPO

1. Get into odtn-emulator_openconfig_cassini_1 container
```
docker exec -it $(docker ps | grep odtn-emulator_openconfig_cassini_1 | cut -d' ' -f1) /bin/bash
```

#### List sysrepo installed YANG modules
```
sysrepoctl -l
```
> Observe column "Module Name". Those names represent installed YANG modules.

#### Get a YANG module with netconf-console (do note use the container shell)
- Explore the ietf-netconf YANG module 
```
netconf-console --host 192.168.15.116 --port 11002 -u root -p root  --get-schema ietf-netconf
```
- Explore the openconfig-platform YANG module 
```
netconf-console --host 192.168.15.116 --port 11002 -u root -p root  --get-schema openconfig-platform
```

# TUTORIAL ROADMAP

- YANG and NETCONF: models, model repository (sysrepo and confd)
- NETCONF Streaming Telemetry, YANG Push
- gNMI: Everything, including streaming telemetry
- humm, let me think


